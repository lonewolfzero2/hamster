﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hamster;
using System.Threading;

namespace hamstertests
{
    class Program
    {
        static void Main(string[] args)
        {
            Cage c = new Cage();

            OneSecondHamster h = new OneSecondHamster(c);


            c.Start();

            Thread.Sleep(10000);
        }
    }

    class OneSecondHamster : Hamster
    {
        
        public OneSecondHamster(Cage c)
        {
            Init(c);
        }

        protected override void Run()
        {
            Console.WriteLine("Waiting 1s...");
            Sleep(1000);
        }

        protected override bool Behavior()
        {
            // always run when awake
            return !IsSleeping();
        }
    }
}
