﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace hamster
{
    //
    // Hamster cage
    // - Single-threaded world to handle one or more hamsters
    // - Public methods are threadsafe
    // - Intentionally Dispose this to destroy thread
    public class Cage : IDisposable
    {
        // hamster wheel
        private Queue<Hamster> wheel = new Queue<Hamster>();

        bool running;

        private Thread thread;

        //
        // Ctor, Dtor
        //

        public Cage()
        {
            running = true;
            thread = new Thread(Run);
        }

        public void Dispose() {
            running = false;
        }



        //
        // Public methods
        //

        // Adds a hamster to the wheel
        public void Spin(Hamster h)
        {
            lock (wheel)
            {
                // ensure hamster doesnt exist in wheel yet
                if (wheel.Contains(h)) return;
                wheel.Enqueue(h);
                Monitor.Pulse(wheel);
            }
        }

        public void Start()
        {
            thread.Start();
        }



        //
        // Private methods
        //

        private void Run()
        {
            while (running)
            {
                Hamster h = null;

                lock (wheel)
                {
                    if (wheel.Count == 0)
                    {
                        Monitor.Wait(wheel);
                        continue;
                    }
                    else
                    {
                        h = wheel.Dequeue();
                    }
                }

                if (h == null) continue;

                h._DoRun();
            }
        }



    }
}
