﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace hamster
{
    //
    // Fundamental task definition
    // - NOT threadsafe
    public abstract class Hamster
    {

        // Temporary storage (buffers incoming data)
        private Dictionary<String, Object> storage_temp = new Dictionary<String, Object>();
        // Permanent storage (after incoming data is committed)
        private Dictionary<String, Object> storage_perm = new Dictionary<String, Object>();

        // Cage which this belongs to
        Cage hamsterCage;

        // Each hammie tracks its own time
        Timer clock;



        protected void Init(Cage c)
        {
            hamsterCage = c;

            clock = new Timer();
            clock.AutoReset = false;
            clock.Enabled = false;
            clock.Elapsed += new ElapsedEventHandler((obj, args) =>
            {
                clock.Enabled = false;
                Poke(); // might have other conditions (apart from timer elapsed) to check
            });

            // Self run, if it should
            Poke();
        }

        // Tries to obtain a data object
        protected object GetFood(string key)
        {
            object o = storage_temp[key];
            if (o == null)
            {
                o = storage_perm[key];
            }
            return o;
        }

        // Sets a data object into temp storage
        protected void SetFood(string key, object val)
        {
            storage_temp[key] = val;
        }

        // Checks if a type of object has been fed
        protected bool HasFood(string key)
        {
            if(storage_temp.ContainsKey(key)) return true;
            return false;
        }

        // Commits temp storage to perm storage
        private void CommitFood()
        {
            foreach (var i in storage_temp)
            {
                storage_perm[i.Key] = i.Value;
            }
            storage_temp.Clear();
        }

        // true if hammie is sleeping
        protected bool IsSleeping()
        {
            return clock.Enabled;
        }

        // Defines how long hammie will sleep from this point onwards
        protected void Sleep(int milliseconds)
        {
            clock.Interval = milliseconds;
            clock.Enabled = true;
        }

        // Defines when hammie will sleep until
        protected void SleepUntil(DateTime t)
        {
            long interval = DateTime2Ms(t) - Now();
            if (interval < 1) throw new Exception("Invalid SleepUntil interval for " + t);
            clock.Interval = interval;
            clock.Enabled = true;
        }




        //
        // Public
        //

        // NOTE: Only called from Cage
        public Boolean _DoRun()
        {
            Run();
            CommitFood();
            return true;
        }
        
        // Check if hamster should be enqueued to run
        public bool Poke()
        {
            if (!Behavior()) return false;
            hamsterCage.Spin(this);
            return true;
        }

        // Feed this hamster something to consume
        public void Feed(Dictionary<String, Object> food)
        {
            foreach (var i in food)
            {
                storage_temp[i.Key] = i.Value;
            }

            Poke(); // It (unfortunately) is woken up too
        }
        
        



        //
        // User defined
        //

        // Work to do
        abstract protected void Run();

        // Conditions for running
        // - e.g. define a series of conditions comprising of HasFood(x)
        abstract protected bool Behavior();


        


        //
        // Helpers
        //

        // Milliseconds timestamp
        private static long Now()
        {
            return DateTime.UtcNow.Ticks / 10000;
        }

        private static long DateTime2Ms(DateTime dt)
        {
            return dt.ToUniversalTime().Ticks / 10000;
        }


    }
}
